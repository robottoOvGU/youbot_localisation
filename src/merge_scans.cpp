// %Tag(FULLTEXT)%
#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include "std_msgs/String.h"
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include "tf/transform_listener.h"
#include "sensor_msgs/PointCloud.h"
#include "tf/message_filter.h"
#include "laser_geometry/laser_geometry.h"
#include <message_filters/sync_policies/approximate_time.h>
#include "sensor_msgs/point_cloud_conversion.h"


using namespace sensor_msgs;
using namespace message_filters;
/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */


class MergeLaserScans{
public:
    ros::NodeHandle n_;
    laser_geometry::LaserProjection projector_;
    tf::TransformListener listener_;
    message_filters::Subscriber<sensor_msgs::LaserScan> *laser_front;
    message_filters::Subscriber<sensor_msgs::LaserScan> *laser_back;
    tf::MessageFilter<sensor_msgs::LaserScan> *tf_laser_front;

    typedef sync_policies::ApproximateTime<sensor_msgs::LaserScan, sensor_msgs::LaserScan> MySyncPolicy;
    message_filters::Synchronizer<MySyncPolicy> *sync;
    ros::Publisher scan_pub_;

    MergeLaserScans(ros::NodeHandle n)
      {
        n_=n;
        laser_front= new message_filters::Subscriber<sensor_msgs::LaserScan>(n_, "hokuyo_right", 1);

        laser_back = new message_filters::Subscriber<sensor_msgs::LaserScan>(n_, "hokuyo_left", 1);

        tf_laser_front = new tf::MessageFilter<sensor_msgs::LaserScan>(*laser_front, listener_, "base_footprint", 10);

        typedef sync_policies::ApproximateTime<sensor_msgs::LaserScan, sensor_msgs::LaserScan> MySyncPolicy;

        sync= new message_filters::Synchronizer<MySyncPolicy>(MySyncPolicy(10), *tf_laser_front,  *laser_back );
        sync->registerCallback(boost::bind(&MergeLaserScans::scanCallback, this, _1, _2));
        scan_pub_ = n_.advertise<sensor_msgs::PointCloud2>("/my_cloud",1);
      }


      void scanCallback (const sensor_msgs::LaserScan::ConstPtr& front, const sensor_msgs::LaserScan::ConstPtr& back)
      {
        sensor_msgs::PointCloud cloud_front;
        sensor_msgs::PointCloud cloud_back;
        sensor_msgs::PointCloud2 result;
        try
        {
            listener_.waitForTransform("right_laser","base_footprint",ros::Time(0),ros::Duration(1));
            projector_.transformLaserScanToPointCloud(
              "base_footprint",*front, cloud_front,listener_);
            listener_.waitForTransform("left_laser","base_footprint",ros::Time(0),ros::Duration(1));
            projector_.transformLaserScanToPointCloud(
              "base_footprint",*back,  cloud_back,listener_);
        }
        catch (tf::TransformException& e)
        {
            std::cout << e.what();
            return;
        }
        cloud_front.points.insert(cloud_front.points.end(), cloud_back.points.begin(), cloud_back.points.end());
        sensor_msgs::convertPointCloudToPointCloud2(cloud_front, result);
        scan_pub_.publish(result);

      }
    };


int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line. For programmatic
   * remappings you can use a different version of init() which takes remappings
   * directly, but for most command-line programs, passing argc and argv is the easiest
   * way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "merge_scans");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle node;
  MergeLaserScans rostopic(node);
  /**
   * The subscribe() call is how you tell ROS that you want to receive messages
   * on a given topic.  This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing.  Messages are passed to a callback function, here
   * called chatterCallback.  subscribe() returns a Subscriber object that you
   * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
   * object go out of scope, this callback will automatically be unsubscribed from
   * this topic.
   *
   * The second parameter to the subscribe() function is the size of the message
   * queue.  If messages are arriving faster than they are being processed, this
   * is the number of messages that will be buffered up before beginning to throw
   * away the oldest ones.
   */
// %Tag(SUBSCRIBER)%
//    message_filters::Subscriber<LaserScan> laser_front(node, "hokuyo_front", 1);
//    message_filters::Subscriber<LaserScan> laser_back(node, "hokuyo_back", 1);
//    TimeSynchronizer<LaserScan, LaserScan> sync(laser_front, laser_back, 10);
//    sync.registerCallback(boost::bind(&callback, _1, _2));
// %EndTag(SUBSCRIBER)%

  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   */
// %Tag(SPIN)%
  ros::spin();
// %EndTag(SPIN)%

  return 0;
}
// %EndTag(FULLTEXT)%
